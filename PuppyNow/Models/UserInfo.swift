//
//  UserInfo.swift
//  PuppyNow
//
//  Created by Steven on 7/8/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import Foundation

struct UserInfo {
    var userName: String
    var email: String
    var profileImage: String
}
