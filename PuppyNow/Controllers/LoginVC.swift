//
//  ViewController.swift
//  PuppyNow
//
//  Created by Steven on 6/29/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//swiftlint:disable line_length

import UIKit
import FirebaseAuth

class LoginVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: RoundedButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        hideKeyBoardOnTouch()
        loginButton.disableButton()
        [emailTextField, passwordTextField].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        loginButton.enableButton()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearUserFields()
    }

    @IBAction func forgotCredentials(_ sender: Any) {
        let alert = UIAlertController(title: "Send Email",
                                      message: "Please enter email associated to this account to send email to reset password", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { (_) in
            guard let email = alert.textFields?.first?.text else {
                return
            }
            self.sendPasswordReset(email: email)
        }))
        self.present(alert, animated: true)
    }

    func sendPasswordReset(email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if error == nil {
                self.presentAlertWithTitle(title: "Email Sent", message: "Check your email for a link to reset password")
            } else {
                self.presentAlertWithTitle(title: "Invalid", message: "Account with this email does not exist")
            }
            // Present Alert to check email for password reset link
        }
    }

    @IBAction func loginToApp(_ sender: Any) {
        loginButton.disableButton()
        self.performSegue(withIdentifier: "ToHomeVC", sender: self)
//        AuthService.instance.loginUser(email: emailTextField.text!, password: passwordTextField.text!) { _, error in
//            if error == nil {
//                self.performSegue(withIdentifier: "ToHomeVC", sender: self)
//            } else {
//                self.notifyUserOfEmptyFields()
//            }
//        }
    }

    @objc func editingChanged(_ textField: UITextField) {
        guard
            emailTextField.text != nil, (emailTextField.text!.hasSuffix(".com") && (emailTextField.text!.contains("@"))),
            passwordTextField.text != nil, passwordTextField.text!.count >= 4
            else {
                loginButton.disableButton()
                return
        }
        loginButton.enableButton()
    }

    func notifyUserOfEmptyFields() {

        let alert = UIAlertController(title: "Incorrect User Name or Password",
                                      message: "Try again or reset Password", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }

    private func clearUserFields() {
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is SignUpVC {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }

}

extension UIViewController: UITextFieldDelegate {
    
    public func hideKeyBoardOnTouch(){
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
