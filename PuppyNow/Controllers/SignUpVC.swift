//
//  SignUpVC.swift
//  PuppyNow
//
//  Created by Steven on 7/1/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//swiftlint:disable line_length

import UIKit
import Firebase

class SignUpVC: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var continueBtn: RoundedButton!

    override func viewDidLoad() {
        
        self.userName.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        hideKeyBoardOnTouch()
        
        continueBtn.disableButton()
        super.viewDidLoad()
    }

    //Can possibly moves this over to viewDidLoad if the view controller will be dismissed and always load back
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        [userName, emailTextField, passwordTextField].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func continueSignUp(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                print("Success Creating USER Account!!")
                if let userResult = user {
                    self.changeDisplayName(user: userResult)
                }
                self.sendVerificationEmail()
            } else {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }

    @objc func editingChanged(_ textField: UITextField) {
        guard
            userName.text != nil, self.userName.text!.count >= 3,
            emailTextField.text != nil, (emailTextField.text!.hasSuffix(".com") && (emailTextField.text!.contains("@"))),
            passwordTextField.text != nil, passwordTextField.text!.count >= 6
            else {
                continueBtn.disableButton()
                return
        }
        continueBtn.enableButton()
    }

    private func changeDisplayName(user: AuthDataResult) {
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = self.userName.text
        changeRequest?.commitChanges(completion: { (error) in
            if error == nil {
                print("User display name changed")
                DataService.instance.createUser(uid: user.user.uid, userData: ["userName": self.userName.text!])
            } else {
                self.presentAlertWithTitle(title: "Error changing user name", message: error.debugDescription)
            }
            self.performSegue(withIdentifier: "SignUpToHomeVC", sender: self)
        })
    }

    private func sendVerificationEmail() {
        Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
            if error != nil {
                self.presentAlertWithTitle(title: "Error sending verification email", message: error.debugDescription)
            } else {
                print("Sending email verification!")
            }
        })
    }
}
