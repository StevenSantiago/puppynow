//
//  DashboardVC.swift
//  PuppyNow
//
//  Created by Steven Santiago on 9/15/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import UIKit
import Firebase

class DashboardVC: UIViewController {

    @IBOutlet weak var welcomeMessage: UILabel!

    var handle: AuthStateDidChangeListenerHandle?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                let userID = Auth.auth().currentUser?.uid
                DataService.instance.GRefUsers.child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
                    // Get user value
                    let value = snapshot.value as? NSDictionary
                    let username = value?["userName"] as? String ?? ""
                    self.welcomeMessage.text = "Welcome back \(username)"
                }) { (error) in
                    print(error.localizedDescription)
                }
               // self.welcomeMessage.text = "Welcome back \(user.displayName)"
            }
        }

    }

}
