//
//  AccountVC.swift
//  PuppyNow
//
//  Created by Steven Santiago on 11/11/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import UIKit

class AccountVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var petNameTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var profilePicture: RoundedImageView!
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        emailTextField.delegate = self
        petNameTextField.delegate = self
        userNameTextField.delegate = self
        hideKeyBoardOnTouch()
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        var newImage: UIImage
        if let possibleImage = info[.editedImage] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info[.originalImage] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }

        // do something interesting here!
        print(newImage.size)
        profilePicture.image = newImage

        dismiss(animated: true)
    }

    @IBAction func logOutPressed(_ sender: Any) {
        if AuthService.instance.signOutUser() == true {
            _ = navigationController?.popToRootViewController(animated: true)
        } else {
            print("There was an error logging out")
        }

    }

    @IBAction func loadImage(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }

}
