//
//  DataService.swift
//Singleton class to access database 
//  PuppyNow
//
//  Created by Steven on 7/13/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import Foundation
import Firebase

let dbBase = Database.database().reference()

class DataService {
    static let instance = DataService()

    private var refBase = dbBase
    //creating users folder if it does not exist or pull existing data
    private var refUsers = dbBase.child("Users")

    var GRefBase: DatabaseReference {
        return refBase
    }

    var GRefUsers: DatabaseReference {
        return refUsers
    }

    func createUser(uid: String, userData: [String: Any]) {
        GRefUsers.child(uid).updateChildValues(userData)
    }
}
