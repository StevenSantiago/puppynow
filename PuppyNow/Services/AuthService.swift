//
//  AuthService.swift
//  PuppyNow
//
//  Created by Steven on 7/17/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//swiftlint:disable line_length

import Foundation
import Firebase

class AuthService {
    static let instance = AuthService()

    func registerUser(email: String, password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error? ) -> Void ) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                userCreationComplete(false, error)
                return
            }

            let userData = ["provider": user.user.providerID, "email": user.user.email]
            DataService.instance.createUser(uid: user.user.uid, userData: userData as [String: Any])
            userCreationComplete(true, nil)
        }
    }

    func loginUser(email: String, password: String, loginComplete: @escaping (_ status: Bool, _ error: Error? ) -> Void ) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            guard user != nil else {
                loginComplete(false, error)
                return
            }
            //print(user)
            loginComplete(true, nil)
        }
    }

    // Possibly broken, It allows a signout even if it did not initially sign out successfully. This happens when creating user was broken
    @discardableResult
    @objc func signOutUser() -> Bool {
        do {
            try Auth.auth().signOut()
            print("Logging out is successfull")
            return true

        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
            return false
        }

    }
}
