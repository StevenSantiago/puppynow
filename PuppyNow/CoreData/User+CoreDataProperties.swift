//
//  User+CoreDataProperties.swift
//  PuppyNow
//
//  Created by Steven Santiago on 9/16/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//
//

import Foundation
import CoreData

extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?

}
