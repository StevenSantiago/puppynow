//
//  Extension.swift
//  PuppyNow
//
//  Created by Steven Santiago on 9/22/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAlertWithTitle(title: String, message: String) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
}
