//
//  RoundedImageView.swift
//  PuppyNow
//
//  Created by Steven Santiago on 11/11/19.
//  Copyright © 2019 Steven Santiago. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {

    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = false
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.white.cgColor
        self.clipsToBounds = true
    }

}
